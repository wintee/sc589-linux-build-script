# USER ENVIRONMENT: You may want to change these
export BUILD_TARGET=sc589
export BUILD_BOARD=ezkit
export LINUX_ADDIN_VERSION=1.2.0
export CCES_VERSION=2.6.0
export TFTP_DIR=/tftpboot

# CONSTANT ENVIRONMENT: You probably don't want to change these
export TD_SRC=`pwd`
export LINUX_ADDIN_ROOT=/opt/analog/cces-linux-add-in/${LINUX_ADDIN_VERSION}
export CCES_ROOT=/opt/analog/cces/${CCES_VERSION}
export PATH=${PATH}:${CCES_ROOT}/ARM/arm-none-eabi/bin:${LINUX_ADDIN_ROOT}/ARM/arm-linux-gnueabi/biN
# uBoot
export UBOOT_ROOT=${LINUX_ADDIN_ROOT}/uboot-sc5xx-${LINUX_ADDIN_VERSION}
export UBOOT_DIR=uboot
# linux
export BUILDROOT_ROOT=${LINUX_ADDIN_ROOT}/buildroot-sc5xx-${LINUX_ADDIN_VERSION}
export BUILDROOT_DIR=buildroot
