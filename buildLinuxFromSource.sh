. ./setupEnv.sh

BUILDROOT_PATCH_DIR=${TD_SRC}/buildroot_patches

if [ ! -d ${BUILDROOT_DIR} ]
then
  echo "No local buildroot directory found so I'm going to create one"
  echo "Unpacking buildroot sources"
  tar -xzf ${BUILDROOT_ROOT}/src/buildroot-sc5xx-${LINUX_ADDIN_VERSION}.tar.gz
  echo "Creating a local repo for convenience"
  cd ${BUILDROOT_DIR}
  git init .
  git add -A * >& /dev/null
  git commit -m "Initial import of sources, as extracted from linux add-in" >& /dev/null
  # See if we need to patch buildroot to build on the host OS
  if [ `lsb_release -r | grep 17.04 | wc -l` -gt 0 ]
  then
    echo "Ah, you're running Ubuntu 17.04. We need to patch buildroot for the build to succeed"
    cp ${BUILDROOT_PATCH_DIR}/0001-gcc6_compatibility.patch package/lzop
    git add package/lzop/0001-gcc6_compatibility.patch
    git commit -m "Added patch to lzop allowing it to build on Ubuntu 17.04"
    cp ${BUILDROOT_PATCH_DIR}/0004-patch-gdate-newer-gcc.patch package/libglib2
    git add package/libglib2/0004-patch-gdate-newer-gcc.patch
    git commit -m "Added patch to libglib2 allowing it to build on Ubuntu 17.04"
  fi
  make clean
  make ${BUILD_TARGET}-${BUILD_BOARD}_defconfig
  git add .config
  git commit -m "Added config file for ${BUILD_TARGET} ${BUILD_BOARD}"
  # As we're targeting running via SD Card we need to patch .Config BEFORE we do any building
  echo "Patching the menuconfig so we can build for SDCard"
  cp ${BUILDROOT_PATCH_DIR}/dotconfig.patch .config
  git add .config
  git commit -m "Reconfigured build to target running via SD Card"
else
  echo "Sources already extracted. I'm not going to wipe them"
  cd ${BUILDROOT_DIR}
fi

# Clean the build and then make
make
echo "Build complete. Hopefully your files are in the ${BUILDROOT_DIR} directory"
cd ..
echo "All Done"
