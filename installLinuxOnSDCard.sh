#!/bin/bash
# Script to install Linux and filesystem onto the SDCard
# Specifically for use with the Analog Devices CrossCore Embededed Studio
# Linux Add-In.
# Copyright Analog Devices 2017.
# This script is licensed under the Analog Devices 4 term BSD License.

# Requires root permissions to do fdisk/mount operations

if [ "`whoami`" != "root" ]
then
  echo "This script requires to be run via sudo"
  exit -1
fi

userDevice=0
userDeviceGiven=0
if [ $# -eq 1 ]
then
  userDevice=$1
  userDeviceGiven=1
fi
. ./setupEnv.sh

kernelImage=${BUILDROOT_DIR}/output/images/uImage
mf=.mountfile
sdDevice=""
sdMountPoint=""

if [ ! -f ${kernelImage} ]
then
  echo "ERROR: This script relies on you having run ./buildLinuxFromSource.sh first"
  echo "If that was successful you should have a kernel image file: ${kernelImage}"
  exit -1
fi

echo "OK, so Buildroot has successfully built a kernel."
echo "Let's see if we can install this onto an SDCard"
echo "Firstly, let's identify your SD Card"
echo "Examing mounted devices..."
if [ ${userDeviceGiven} -eq 0 ]
then
  echo "The script assumes that the last device connected to this host is your SD Card"

  # This is a bit hacky, but essentially Ubuntu auto-mounts every SD Card you connect
  # to the host. So we just look for the last mounted device.
  # We will prompt the user to confirm that this is the SDCard
  mount | tail -1 > ${mf}
  sdDevice=`cat ${mf} | awk '{ print $1}' | sed -e 's/[0-9]*//g'`
  sdMountPoint=`cat ${mf} | awk '{print $3}'`
  echo "Selecting last mounted device"
else
  sdDevice=${userDevice}
  mount | grep "${userDevice}" > ${mf}
  if [ `cat ${mf} | wc -l` -eq 1 ]
  then
    sdMountPoint=`cat ${mf} | awk '{print $3}'`
    echo "Device is currently mounted as ${sdMountPoint}"
  fi
fi
devDone=0
while [ ${devDone} -ne 1 ]
do
  sdSize=`fdisk -l ${sdDevice} | head -1 | awk '{print $3 $4}'`
  echo "Selected Disk: ${sdDevice}"
  echo "    Disk Size: ${sdSize}"
  if [ ${userDeviceGiven} -eq 1 ]
  then
    devDone=1
  else
    echo "Hit enter to select this disk"
    echo "Enter another /dev device to override"
    echo "Or ctrl-c to quit"
    echo -n "    ${sdDevice}:"
    response=""
    read -p " " response
    if [ `echo ${response} | wc -c` -eq 1 ]
    then
      devDone=1
    else
      if [ `file $response | grep "block special" | wc -l` -ne 1 ]
      then
        echo "Error: ${response} is not a valid block device"
      else
        echo "User suggesting ${response} as device. Probing..."
        sdDevice=${response}
      fi
    fi
  fi
done
echo "OK: Using ${sdDevice}. This device will be COMPLETELY FORMATTED and/or wiped"
echo "Ensure that you have backed up any content on this device before the script continues"
sleep 5
# Check to see if the card contains a single Linux partition
if [ "${sdMountPoint}" != "" ]
then
  echo "Unmounting the device..."
  umount ${sdMountPoint}
fi
copyPartition="${sdDevice}1"
copyMountDir="/mnt"
partFile=".partitions"
fdisk -l ${sdDevice} | tail +9 >  ${partFile}
needToPartition=0
# Check that it contains 1 partition
if [ `cat ${partFile} | wc -l` -eq 1 ]
then
  # Is it a Linux partition
  if [[ `cat ${partFile} | awk '{ print $NF}'` == Linux ]]
  then
    echo "SD Card already partitioned for Linux"
    needToPartition=0
  else
    echo "SD Card needs repartitioning for Linux"
    needToPartition=1
  fi
else
  echo "SD Card needs repartitioning for Linux (2)"
  needToPartition=1
fi
if [ ${needToPartition} -eq 1 ]
then
  echo "Need to re-partition the SD Card (fingers crossed)"
  cat fdisk_cmds | fdisk ${sdDevice}
else
  echo "Not partitioning the SD Card"
fi
if [ ! -d ${copyMountDir} ]
then
  mkdir ${copyMountDir}
fi
if [ `mount | grep "${copyMountDir}" | wc -l ` -eq 1 ]
then
  echo "Unmounting existing mount at ${copyMountDir}"
  umount ${copyMountDir}
fi

echo "Creating the Linux filesystem on the SD Card"
mkfs.ext3 ${copyPartition}

mount ${copyPartition} ${copyMountDir}
echo "Creating a new mount point for ${copyPartition} at ${copyMountDir}"
mount | tail -1

# Now let's mount the filesystem from buildroot and copy it onto the SD Card
imageMountDir=${TD_SRC}/sc5xx_rootfs
imageFile=${BUILDROOT_DIR}/output/images/rootfs.ext3
if [ `mount | grep ${imageMountDir} | wc -l` -eq 1 ]
then
  umount ${imageMountDir}
fi
rm -rf ${imageMountDir}
mkdir -p ${imageMountDir}
mount ${imageFile} ${imageMountDir}
echo "Copying filesystem"
cp ${imageMountDir}/* -rf ${copyMountDir}

#install the uImage file on the card
cp ${BUILDROOT_DIR}/output/images/uImage ${copyMountDir}/vmImage-${BUILD_TARGET}-${BUILD_BOARD}
cp ${BUILDROOT_DIR}/output/images/uImage ${TFTPDIR}/vmImage-${BUILD_TARGET}-${BUILD_BOARD}
cp ${BUILDROOT_DIR}/output/images/${BUILD_TARGET}-${BUILD_BOARD}.dtb ${TFTP_DIR}
echo "I have copied the kernel image into both the SD Card and the ${TFTP_DIR} directories."
echo "I have also copied the DTB file to ${TFTP_DIR}."
echo "You will need to tftp/flash a new version of the DTB file to your SPI"
echo "And if you're booting from a kernel in SPI you will need to flash that too"

# Clean up
echo "Cleaning up and ejecting the SD Card. Please wait..."
rm -f ${partFile} ${mf}
umount ${imageMountDir}
umount ${copyMountDir}
rm -rf ${imageMountDir}

echo "All done!"
echo "Eject the card, stick it in the EZ-Kit and see if it works"
echo "If you're using a kernel in SPI, you'll need to flash the kernel and dtb file from within uBoot"
