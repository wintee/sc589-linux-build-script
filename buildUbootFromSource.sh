. ./setupEnv.sh

if [ ! -d ${UBOOT_DIR} ]
then
  echo "No local uBoot directory found so I'm going to create one"
  echo "Unpacking uBoot sources"
  tar -xzf ${UBOOT_ROOT}/src/uboot-sc5xx-${LINUX_ADDIN_VERSION}.tar.gz
  echo "Creating a local repo for convenience"
  cd ${UBOOT_DIR}
  git init .
  git add -A * >& /dev/null
  git commit -m "Initial import of sources, as extracted from linux add-in" >& /dev/null
else
  echo "Sources already extracted. I'm not going to wipe them"
  cd ${UBOOT_DIR}
fi

# Clean the build and then make
echo "Performing a clean build of the sources"
make clean
make ${BUILD_TARGET}-${BUILD_BOARD}_defconfig
make
echo "Build complete. Hopefully your files are in the ${UBOOT_DIR} directory"
cd ..
echo "All Done"
